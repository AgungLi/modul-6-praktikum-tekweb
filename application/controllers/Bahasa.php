<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Bahasa extends CI_Controller
{
    public function index()
    {
        $this->load->model("BahasaModel", "", TRUE);
        $data['Language'] = $this->BahasaModel->getBahasa();
        $this->load->view("bahasa", $data);
    }
}
