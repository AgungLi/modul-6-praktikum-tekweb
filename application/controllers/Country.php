<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Country extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("CityModel", "", TRUE);
        $this->load->model("BahasaModel", "", TRUE);
        $this->load->model("CountryModel", "", TRUE);
    }
    public function index()
    {
        $this->load->model("CityModel", "", TRUE);
        $this->load->model("BahasaModel", "", TRUE);
        $this->load->model("CountryModel", "", TRUE);
        $data['country'] = $this->CountryModel->getCountry();
        $this->load->view("country", $data);
    }

    public function tambah()
    {
        $this->load->model('CountryModel');
        $data['country'] = $this->CountryModel->getCountry();
        $this->load->view("negara_tambah", $data);
    }

    public function prosesTambah()
    {
        if ($this->CountryModel->insertCountry()) {
            redirect(site_url("country"));
        } else {
            redirect(site_url("country/tambah"));
        }
    }

    public function hapus($id)
    {
        $this->CityModel->hapusCity($id);
        $this->BahasaModel->hapusBahasa($id);
        $this->CountryModel->deleteCountry($id);
        redirect(site_url("country"));
    }
}
