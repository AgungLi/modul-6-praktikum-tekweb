<?php
defined('BASEPATH') or exit('No direct script access allowed');
class CountryModel extends CI_Model
{
    public function getCountry()
    {
        return $this->db->get("country");
    }

    public function insertCountry()
    {
        $country = array(
            "Name"   => $this->input->post("name"),
            "Code"   => $this->input->post("code")
        );
        return $this->db->insert('country', $country);
    }

    function getCountryById($id)
    {
        $this->db->where("Code", $id);
        return $this->db->get('country');
    }

    function deleteCountry($id)
    {
        $this->db->where("Code", $id);
        return $this->db->delete("country");
    }
}
