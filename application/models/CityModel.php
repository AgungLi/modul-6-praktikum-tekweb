<?php
defined('BASEPATH') or exit('No direct script access allowed');
class CityModel extends CI_Model
{
    public function getCity()
    {
        return $this->db->get("city");
    }
    public function insertCity()
    {
        $city = array(
            "Name"          => $this->input->post("name"),
            "countryCode"   => $this->input->post("code"),
            "District"   => $this->input->post("area"),
            "Population"   => $this->input->post("populasi")
        );
        return $this->db->insert('City', $city);
    }

    function getCityById($id)
    {
        $this->db->where("ID", $id);
        return $this->db->get('City');
    }

    function updateCity($id)
    {
        $city = array(
            "Name" => $this->input->post("name"),
            "CountryCode" => $this->input->post("code"),
            "District" => $this->input->post("area"),
            "Population" => $this->input->post("populasi")
        );
        $this->db->where("ID", $id);
        return $this->db->update("City", $city);
    }

    function deleteCity($id)
    {
        $this->db->where("ID", $id);
        return $this->db->delete("City");
    }

    function hapusCity($id)
    {
        $this->db->where("CountryCode", $id);
        return $this->db->delete("City");
    }
}
