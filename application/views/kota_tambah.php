<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tambah Kota</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
        label {
            display: inline-block;
            width: 100px;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/style.css">
</head>

<body>

    <section class="container-fluid">
        <section class="row justify-content-center">
            <section class="col-12 col-sm-6 col-mb-3">
                <form action="<?= site_url('city/prosestambah'); ?>" method="post" class="form-container">
                    <h3 class="text-center">Tambah Kota</h3>
                    <div class="form-group">
                        <label for="name">Nama</label>
                        <input type="text" class="form-control" name="name" id="name">
                    </div>
                    <div class="form-group">
                        <label for="code">Code Negara</label>
                        <select name="code">
                            <?php
                            foreach ($country->result() as $ctr) {
                                echo '<option value="' . $ctr->Code . '">' . $ctr->Code . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="area">District</label>
                        <input type="text" class="form-control" name="area" id="area">
                    </div>
                    <div class="form-group">
                        <label for="populasi">Population</label>
                        <input type="text" class="form-control" name="populasi" id="populasi">
                    </div>
                    <button type="submit" class="btn btn-dark btn-block" name="tambah">Tambah Data</button>
                    <a href="<?php echo site_url('city/index') ?>" class="btn btn-dark btn-block">Kembali Ke Halaman Awal</a>
                </form>
            </section>
        </section>
    </section>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>